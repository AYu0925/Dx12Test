#include"Dx12Render.h"
#include <DirectXColors.h>
#include <D3Dcompiler.h>

cDx12Render::cDx12Render()
	: m_iM4XQualityLevels(0)
	, m_bMSAA4XEnabled(false)
	, m_dfBackBufferFormat(DXGI_FORMAT::DXGI_FORMAT_R8G8B8A8_UNORM)
	, m_dfDepthStencilFormat(DXGI_FORMAT::DXGI_FORMAT_D24_UNORM_S8_UINT)
	, m_iCurrentSwapBuffIndex(0)
{

}

cDx12Render::~cDx12Render()
{

}

BOOL cDx12Render::Init(cDx12RenderConfig* io_cDx12RenderConfig)
{
	//Debug
	ComPtr<ID3D12Debug> D3D12Debug;
	if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&D3D12Debug))))
	{
		D3D12Debug->EnableDebugLayer();
	}

	CreateFactory();
	CreateDevice();

	CreateCMDQueueListAlloctor();

	SetMSAA();
	CreateSwapChain(io_cDx12RenderConfig);

	CreateRTVHeapAndResource(io_cDx12RenderConfig->iSwapChainCount);
	
	CreateFence();

	CreateViewPortAndScissorRect(io_cDx12RenderConfig);

	InitAsset();

	return TRUE;
}

void cDx12Render::Draw(float DeltaTime)
{
	ResetCMDListAlloctor();

	//核心点
	SubmitDrawingTask();

	PageTurn();

	//CPU等GPU
	WaitGPUComplete();
}

void cDx12Render::CreateFactory()
{
	//创建DXGI工厂
	CreateDXGIFactory1(IID_PPV_ARGS(&m_spDXGIFactory));
}

void cDx12Render::CreateDevice()
{
	//创建设备
	HRESULT D3dDeviceResult = D3D12CreateDevice(NULL, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&m_spD3dDevice));
	if (FAILED(D3dDeviceResult))
	{
		ComPtr<IDXGIAdapter> spWARPAdapter;
		m_spDXGIFactory->EnumWarpAdapter(IID_PPV_ARGS(&spWARPAdapter));
		D3D12CreateDevice(spWARPAdapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&m_spD3dDevice));
	}
}

void cDx12Render::CreateCMDQueueListAlloctor()
{
	//命令队列的创建
	D3D12_COMMAND_QUEUE_DESC cQueueDesc = {};
	cQueueDesc.Type = D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_DIRECT;
	cQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAGS::D3D12_COMMAND_QUEUE_FLAG_NONE;
	m_spD3dDevice->CreateCommandQueue(&cQueueDesc, IID_PPV_ARGS(&m_spCommandQueue));

	//命令分配器的创建
	m_spD3dDevice->CreateCommandAllocator(
		D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_DIRECT,
		IID_PPV_ARGS(m_spCommandAllocator.GetAddressOf()));

	//命令列表的创建
	m_spD3dDevice->CreateCommandList(
		0, //默认单个Gpu 
		D3D12_COMMAND_LIST_TYPE::D3D12_COMMAND_LIST_TYPE_DIRECT,
		m_spCommandAllocator.Get(),//将Commandlist关联到Allocator
		NULL,//ID3D12PipelineState
		IID_PPV_ARGS(m_spGraphicsCommandList.GetAddressOf()));

	m_spGraphicsCommandList->Close();
}

void cDx12Render::CreateFence()
{
	//创建围栏
	m_spD3dDevice->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&m_spFence));
}

void cDx12Render::SetMSAA()
{
	D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS cQualityLevels;
	cQualityLevels.Format = m_dfBackBufferFormat;
	cQualityLevels.SampleCount = 4;
	cQualityLevels.Flags = D3D12_MULTISAMPLE_QUALITY_LEVEL_FLAGS::D3D12_MULTISAMPLE_QUALITY_LEVELS_FLAG_NONE;
	cQualityLevels.NumQualityLevels = 0;

	m_spD3dDevice->CheckFeatureSupport(
		D3D12_FEATURE_MULTISAMPLE_QUALITY_LEVELS,
		&cQualityLevels,
		sizeof(cQualityLevels));

	m_iM4XQualityLevels = cQualityLevels.NumQualityLevels;
}

void cDx12Render::CreateSwapChain(cDx12RenderConfig* io_cDx12RenderConfig)
{
	m_spSwapChain.Reset();

	DXGI_SWAP_CHAIN_DESC cSwapChainDesc;
	cSwapChainDesc.BufferDesc.Width = io_cDx12RenderConfig->iResolutionWidth;//分辨率宽度
	cSwapChainDesc.BufferDesc.Height = io_cDx12RenderConfig->iResolutionHight;//分辨率高
	cSwapChainDesc.BufferDesc.RefreshRate.Numerator = io_cDx12RenderConfig->iRefreshRate;//刷新率
	cSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	cSwapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER::DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	cSwapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	cSwapChainDesc.BufferCount = io_cDx12RenderConfig->iSwapChainCount;
	cSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;//使用表面或资源作为输出渲染目标。
	cSwapChainDesc.OutputWindow = io_cDx12RenderConfig->hHandle;//指定windows句柄
	cSwapChainDesc.Windowed = true;//以窗口运行
	cSwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT::DXGI_SWAP_EFFECT_FLIP_DISCARD;
	cSwapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG::DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;//IDXGISwapChain::ResizeTarget
	cSwapChainDesc.BufferDesc.Format = m_dfBackBufferFormat;//纹理格式

	//多重采样设置
	cSwapChainDesc.SampleDesc.Count = m_bMSAA4XEnabled ? 4 : 1;
	cSwapChainDesc.SampleDesc.Quality = m_bMSAA4XEnabled ? (m_iM4XQualityLevels - 1) : 0;
	m_spDXGIFactory->CreateSwapChain(
		m_spCommandQueue.Get(),
		&cSwapChainDesc, m_spSwapChain.GetAddressOf());

	m_spSwapChain->ResizeBuffers(
		io_cDx12RenderConfig->iSwapChainCount,
		io_cDx12RenderConfig->iScrrenWidth,		
		io_cDx12RenderConfig->iScrrenHight,
		m_dfBackBufferFormat, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH);
}

void cDx12Render::CreateRTVHeapAndResource(int i_iSwapChainCount)
{
	//创建RTV描述符堆
	D3D12_DESCRIPTOR_HEAP_DESC cRTVDescriptorHeapDesc;
	cRTVDescriptorHeapDesc.NumDescriptors = i_iSwapChainCount;
	cRTVDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	cRTVDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	cRTVDescriptorHeapDesc.NodeMask = 0;
	m_spD3dDevice->CreateDescriptorHeap(
		&cRTVDescriptorHeapDesc,
		IID_PPV_ARGS(m_spRTVHeap.GetAddressOf()));

	for (int i = 0; i < i_iSwapChainCount; i++)
	{
		vecRTVBuffer.push_back(ComPtr<ID3D12Resource>());
	}

	//拿到描述size
	m_iRTVDescriptorSize = m_spD3dDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

	CD3DX12_CPU_DESCRIPTOR_HANDLE cRTVHeapHandle(m_spRTVHeap->GetCPUDescriptorHandleForHeapStart());
	for (int i = 0; i < i_iSwapChainCount; i++)
	{
		m_spSwapChain->GetBuffer(i, IID_PPV_ARGS(&vecRTVBuffer[i]));

		//RTV描述符放入RTV描述符堆中
		m_spD3dDevice->CreateRenderTargetView(vecRTVBuffer[i].Get(), nullptr, cRTVHeapHandle);
		cRTVHeapHandle.Offset(1, m_iRTVDescriptorSize);
	}
}

void cDx12Render::CreateViewPortAndScissorRect(cDx12RenderConfig* io_cDx12RenderConfig)
{
	//描述视口尺寸
	m_dvViewprotInfo.TopLeftX = 0;
	m_dvViewprotInfo.TopLeftY = 0;
	m_dvViewprotInfo.Width = io_cDx12RenderConfig->iScrrenWidth;
	m_dvViewprotInfo.Height = io_cDx12RenderConfig->iScrrenHight;
	m_dvViewprotInfo.MinDepth = 0.f;
	m_dvViewprotInfo.MaxDepth = 1.f;

	//矩形
	m_drViewprotRect.left = 0;
	m_drViewprotRect.top = 0;
	m_drViewprotRect.right = io_cDx12RenderConfig->iScrrenWidth;
	m_drViewprotRect.bottom = io_cDx12RenderConfig->iScrrenHight;
}

void cDx12Render::ResetCMDListAlloctor()
{
	//重置录制相关的内存，为下一帧做准备
	m_spCommandAllocator->Reset();

	m_spGraphicsCommandList->Reset(m_spCommandAllocator.Get(), m_pipelineState.Get());
}

void cDx12Render::SubmitDrawingTask()
{
	//把后缓冲区的资源状态切换成Render Target
	CD3DX12_RESOURCE_BARRIER ResourceBarrierPresent = CD3DX12_RESOURCE_BARRIER::Transition(
		vecRTVBuffer[m_iCurrentSwapBuffIndex].Get(),
		D3D12_RESOURCE_STATE_PRESENT, 
		D3D12_RESOURCE_STATE_RENDER_TARGET);

	m_spGraphicsCommandList->ResourceBarrier(1, &ResourceBarrierPresent);

	//设置视口和裁剪区域。
	m_spGraphicsCommandList->RSSetViewports(1, &m_dvViewprotInfo);
	m_spGraphicsCommandList->RSSetScissorRects(1, &m_drViewprotRect);

	//清空后缓存和深度缓存。
	m_spGraphicsCommandList->ClearRenderTargetView(
		CD3DX12_CPU_DESCRIPTOR_HANDLE(
			m_spRTVHeap->GetCPUDescriptorHandleForHeapStart(),
			m_iCurrentSwapBuffIndex,
			m_iRTVDescriptorSize),
		DirectX::Colors::Blue,
		0, 
		nullptr);

	//输出的合并阶段
	D3D12_CPU_DESCRIPTOR_HANDLE SwapBufferView = CD3DX12_CPU_DESCRIPTOR_HANDLE(
		m_spRTVHeap->GetCPUDescriptorHandleForHeapStart(),
		m_iCurrentSwapBuffIndex,
		m_iRTVDescriptorSize);

	//D3D12_CPU_DESCRIPTOR_HANDLE DepthStencilView = m_spDSVHeap->GetCPUDescriptorHandleForHeapStart();

	m_spGraphicsCommandList->OMSetRenderTargets(
		1,
		&SwapBufferView,
		true,
		nullptr);

	SubmitDrawingTaskCore();

	//把后缓冲区切换成PRESENT状态
	CD3DX12_RESOURCE_BARRIER ResourceBarrierPresentRenderTarget = CD3DX12_RESOURCE_BARRIER::Transition(
		vecRTVBuffer[m_iCurrentSwapBuffIndex].Get(),
		D3D12_RESOURCE_STATE_RENDER_TARGET,
		D3D12_RESOURCE_STATE_PRESENT);

	m_spGraphicsCommandList->ResourceBarrier(1, &ResourceBarrierPresentRenderTarget);

	//录入完成
	m_spGraphicsCommandList->Close();

	//提交命令
	ID3D12CommandList* pCommandList[] = { m_spGraphicsCommandList.Get() };
	m_spCommandQueue->ExecuteCommandLists(_countof(pCommandList), pCommandList);
}

void cDx12Render::PageTurn()
{
	//交换两个buff缓冲区
	m_spSwapChain->Present(0, 0);
	m_iCurrentSwapBuffIndex = !(bool)m_iCurrentSwapBuffIndex;
}

void cDx12Render::WaitGPUComplete()
{
	m_iCurFenceIndex++;

	//向GUP设置新的隔离点 等待GPU处理玩信号
	m_spCommandQueue->Signal(m_spFence.Get(), m_iCurFenceIndex);

	if (m_spFence->GetCompletedValue() < m_iCurFenceIndex)
	{
		HANDLE hEventEX = CreateEventEx(NULL, NULL, 0, EVENT_ALL_ACCESS);

		//GPU完成后会通知我们的Handle
		m_spFence->SetEventOnCompletion(m_iCurFenceIndex, hEventEX);

		//等待GPU,阻塞主线程
		WaitForSingleObject(hEventEX, INFINITE);
		CloseHandle(hEventEX);
	}
}

void cDx12Render::InitAsset()
{
	BuildMeshData();

	BuildPSO();
}

void cDx12Render::BuildMeshData()
{
	/*cVertex triangleVertices[] =
	{
		{ { 0.0f, 0.5f, 0.0f }, XMFLOAT4(Colors::Red) },
		{ { 0.5f, -0.5f, 0.0f }, XMFLOAT4(Colors::Green)},
		{ { -0.5f, -0.5f, 0.0f }, XMFLOAT4(Colors::Blue) }
	};*/

	cVertex vertices[] =
	{
		{ { -0.5f, -0.5f, -0.5f }, XMFLOAT4(Colors::Red) },     // left-bottom-front
		{ { -0.5f, 0.5f, -0.5f }, XMFLOAT4(Colors::Yellow) },   // left-up-front
		{ { 0.0f, 0.5f, -0.5f }, XMFLOAT4(Colors::Green) },     // right-up-front
		{ { 0.0f, -0.5f, -0.5f }, XMFLOAT4(Colors::Orange) },   // right-bottom-front
		{ { -0.5f, -0.5f, 0.5f }, XMFLOAT4(Colors::Pink) },     // left-bottom-back
		{ { -0.5f, 0.5f, 0.5f }, XMFLOAT4(Colors::Blue) },      // left-up-back
		{ { 0.0f, 0.5f, 0.5f }, XMFLOAT4(Colors::Black) },      // right-up-back
		{ { 0.0f, -0.5f, 0.5f }, XMFLOAT4(Colors::White) },     // right-bottom-back
	};

	cVertex cubeVertices[] =
	{
		// front face
		vertices[0], vertices[1], vertices[2],
		vertices[0], vertices[2], vertices[3],
		// back face
		vertices[4], vertices[6], vertices[5],
		vertices[4], vertices[7], vertices[6],
		// left face
		vertices[4], vertices[5], vertices[1],
		vertices[4], vertices[1], vertices[0],
		// right face
		vertices[3], vertices[2], vertices[6],
		vertices[3], vertices[6], vertices[7],
		// top face
		vertices[1], vertices[5], vertices[6],
		vertices[1], vertices[6], vertices[2],
		// bottom face
		vertices[4], vertices[0], vertices[3],
		vertices[4], vertices[3], vertices[7],
	};
	
	ComPtr<ID3D12Resource> vertexUploadBuffer;

	const UINT triangleVerticesSize = sizeof(cubeVertices);
	CD3DX12_RESOURCE_DESC BufferResourceDESC = CD3DX12_RESOURCE_DESC::Buffer(triangleVerticesSize);
	CD3DX12_HEAP_PROPERTIES BufferProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
	
	m_spD3dDevice->CreateCommittedResource(
		&BufferProperties,
		D3D12_HEAP_FLAG_NONE,
		&BufferResourceDESC,
		D3D12_RESOURCE_STATE_COMMON,
		nullptr,
		IID_PPV_ARGS(&m_vertexBuffer));

	CD3DX12_HEAP_PROPERTIES UpdateBufferProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
	m_spD3dDevice->CreateCommittedResource(
		&UpdateBufferProperties,
		D3D12_HEAP_FLAG_NONE,
		&BufferResourceDESC,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&vertexUploadBuffer));

	// 设置要传输的CPU数据
	D3D12_SUBRESOURCE_DATA subResourceData = {};
	subResourceData.pData = cubeVertices;
	subResourceData.RowPitch = triangleVerticesSize;
	subResourceData.SlicePitch = subResourceData.RowPitch;

	m_spGraphicsCommandList->Reset(m_spCommandAllocator.Get(), nullptr); // 之前close了command list，这里要使用到，需要reset操作才可记录command

	CD3DX12_RESOURCE_BARRIER CopyDestBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_vertexBuffer.Get(),
		D3D12_RESOURCE_STATE_COMMON,
		D3D12_RESOURCE_STATE_COPY_DEST);

	//传输前要更改resource的state
	m_spGraphicsCommandList->ResourceBarrier(1, &CopyDestBarrier);

	//会先将CPU内存数据拷贝到upload heap中，然后再通过ID3D12CommandList::CopySubresourceRegion从upload heap中拷贝到default buffer中
	UpdateSubresources<1>(
		m_spGraphicsCommandList.Get(),
		m_vertexBuffer.Get(),
		vertexUploadBuffer.Get(),
		0,
		0,
		1,
		&subResourceData);

	CD3DX12_RESOURCE_BARRIER ReadDestBarrier = CD3DX12_RESOURCE_BARRIER::Transition(m_vertexBuffer.Get(),
		D3D12_RESOURCE_STATE_COPY_DEST,
		D3D12_RESOURCE_STATE_GENERIC_READ);


	m_spGraphicsCommandList->Close();
	ID3D12CommandList* cmdsLists[] = { m_spGraphicsCommandList.Get() };
	m_spCommandQueue->ExecuteCommandLists(_countof(cmdsLists), cmdsLists);
	WaitGPUComplete();

	
	m_vertexBufferView.BufferLocation = m_vertexBuffer->GetGPUVirtualAddress();
	m_vertexBufferView.SizeInBytes = triangleVerticesSize;
	m_vertexBufferView.StrideInBytes = sizeof(cVertex);
}

void cDx12Render::BuildPSO()
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc = {};

	//InputLayout
	D3D12_INPUT_ELEMENT_DESC inputElementDescs[] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
	};
	D3D12_INPUT_LAYOUT_DESC inputLayout = { inputElementDescs, _countof(inputElementDescs) };
	psoDesc.InputLayout = inputLayout;

	//Root Signature
	CD3DX12_ROOT_SIGNATURE_DESC rootSignatureDesc;
	rootSignatureDesc.Init(0, nullptr, 0, nullptr, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

	ComPtr<ID3DBlob> signature, error;
	D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error);

	ComPtr<ID3D12RootSignature> rootSignature;
	m_spD3dDevice->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&rootSignature));
	psoDesc.pRootSignature = rootSignature.Get();

	//Shader Compiler
	ComPtr<ID3DBlob> vsByteCode, psByteCode;

#if defined(_DEBUG)
	UINT compileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
	UINT compileFlags = 0;
#endif

	D3DCompileFromFile(L"E:/Code/Dx12Test0302/Dx12Text/Debug/shaders.hlsl", nullptr, nullptr, "VSMain", "vs_5_0", compileFlags, 0, &vsByteCode, nullptr);
	D3DCompileFromFile(L"E:/Code/Dx12Test0302/Dx12Text/Debug/shaders.hlsl", nullptr, nullptr, "PSMain", "ps_5_0", compileFlags, 0, &psByteCode, nullptr);

	//绑定顶点着色器代码
	psoDesc.VS.pShaderBytecode = reinterpret_cast<BYTE*>(vsByteCode->GetBufferPointer());
	psoDesc.VS.BytecodeLength = vsByteCode->GetBufferSize();

	//绑定像素着色器
	psoDesc.PS.pShaderBytecode = psByteCode->GetBufferPointer();
	psoDesc.PS.BytecodeLength = psByteCode->GetBufferSize();

	//Other Setting
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState.StencilEnable = FALSE;
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = 1;
	psoDesc.RTVFormats[0] = m_dfBackBufferFormat;
	//psoDesc.DSVFormat = m_dfDepthStencilFormat;
	psoDesc.SampleDesc.Count = 2;

	m_spD3dDevice->CreateGraphicsPipelineState(&psoDesc, IID_PPV_ARGS(&m_pipelineState));
}

void cDx12Render::SubmitDrawingTaskCore()
{
	m_spGraphicsCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	m_spGraphicsCommandList->IASetVertexBuffers(0, 1, &m_vertexBufferView);

	m_spGraphicsCommandList->DrawInstanced(36, 1, 0, 0);
}