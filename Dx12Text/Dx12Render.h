#include"RenderData.h"
#include"Dx12RenderConfig.h"

class cDx12Render
{
public:
	cDx12Render();
	~cDx12Render();

public:
	BOOL Init(cDx12RenderConfig* io_cDx12RenderConfig);
	void Draw(float DeltaTime);

private:
	void CreateFactory();
	void CreateDevice();
	void CreateCMDQueueListAlloctor();
	void CreateFence();
	void SetMSAA();
	void CreateSwapChain(cDx12RenderConfig* io_cDx12RenderConfig);
	void CreateRTVHeapAndResource(int i_iSwapChainCount);

	void CreateViewPortAndScissorRect(cDx12RenderConfig* io_cDx12RenderConfig);

	void ResetCMDListAlloctor();
	void SubmitDrawingTask();
	void PageTurn();
	void WaitGPUComplete();

	void InitAsset();
	void BuildMeshData();
	void BuildPSO();

	void SubmitDrawingTaskCore();

public:

private:
	cDx12RenderConfig cMyDx12RenderConfig;

	ComPtr<IDXGIFactory4> m_spDXGIFactory;//创建 DirectX 图形基础结构 (DXGI) 对象
	ComPtr<ID3D12Device> m_spD3dDevice;//创建命令分配器、命令列表、命令队列、Fence、资源、管道状态对象、堆、根签名、采样器和许多资源视图

	ComPtr<ID3D12CommandQueue> m_spCommandQueue;//队列
	ComPtr<ID3D12CommandAllocator> m_spCommandAllocator; //存储
	ComPtr<ID3D12GraphicsCommandList> m_spGraphicsCommandList;//命令列表

	ComPtr<IDXGISwapChain> m_spSwapChain;

	ComPtr<ID3D12DescriptorHeap> m_spRTVHeap;
	ComPtr<ID3D12DescriptorHeap> m_spDSVHeap;

	ComPtr<ID3D12Fence> m_spFence;//一个用于同步 CPU 和一个或多个 GPU 的对象。
	std::vector<ComPtr<ID3D12Resource>> vecRTVBuffer;
	ComPtr<ID3D12Resource> m_spDepthStencilBuffer;

	//和屏幕的视口有关
	D3D12_VIEWPORT m_dvViewprotInfo;
	D3D12_RECT m_drViewprotRect;

	ComPtr<ID3D12Resource> m_vertexBuffer;
	D3D12_VERTEX_BUFFER_VIEW m_vertexBufferView;
	ComPtr<ID3D12PipelineState> m_pipelineState;

private:
	UINT m_iM4XQualityLevels;
	bool m_bMSAA4XEnabled;
	DXGI_FORMAT m_dfBackBufferFormat;
	DXGI_FORMAT m_dfDepthStencilFormat;
	UINT m_iRTVDescriptorSize;

	UINT64 m_iCurFenceIndex;
	int m_iCurrentSwapBuffIndex;
};

