#include"Core.h"
#include"Dx12RenderConfig.h"

struct cVertex
{
	cVertex(const XMFLOAT3& InPos, const XMFLOAT4& InColor)
	{
		m_mPosition = InPos;
		m_mColor = InColor;
	}

	XMFLOAT3 m_mPosition;
	XMFLOAT4 m_mColor;
};

struct RenderingData
{
	vector<cVertex> VertexData;
	vector<uint16_t> IndexData;
};

