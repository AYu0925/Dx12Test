#include "Dx12Window.h"
#include "Dx12Render.h"

int WINAPI WinMain(
	HINSTANCE i_hInstance,		//应用程序当前运行的实例的句柄，这是系统给该实例的一个唯一标识符，是一个数值
	HINSTANCE i_PrevInstance,	//当前实例的前一个实例的句柄。在Win32环境下，这个参数总是NULL
	PSTR i_lpCmdLine,				//一个以空终止的字符串，指定传递给应用程序的命令行参数,可在工程设置中调整
	int i_nCmdShow				//窗口如何显示
)
{
	cDx12RenderConfig cMyDx12RenderConfig;
	cMyDx12RenderConfig.iScrrenLeftX = 250;
	cMyDx12RenderConfig.iScrrenLeftY = 150;
	cMyDx12RenderConfig.iScrrenWidth = 1000;
	cMyDx12RenderConfig.iScrrenHight = 600;
	cMyDx12RenderConfig.iRefreshRate = 60;
	cMyDx12RenderConfig.iSwapChainCount = 2;
	cMyDx12RenderConfig.iResolutionWidth = 1920;
	cMyDx12RenderConfig.iResolutionHight = 1080;

	if (!CreateDx12Window(i_hInstance, &cMyDx12RenderConfig))
	{
		return 1;
	}

	cDx12Render cDx12Render;
	if (!cDx12Render.Init(&cMyDx12RenderConfig))
	{
		return 1;
	}

	MSG EngineMsg = { 0 };

	while (true)
	{
		if (PeekMessage(&EngineMsg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&EngineMsg);
			DispatchMessage(&EngineMsg);
		}
		else
		{
			cDx12Render.Draw(0.03);
		}
	}

	system("pause");//暂停一下

	return 1;
}