#pragma once
#include"Core.h"

struct cDx12RenderConfig
{
	HWND hHandle;

	int iScrrenLeftX;
	int iScrrenLeftY;

	int iScrrenWidth;
	int iScrrenHight;

	int iRefreshRate;
	int iSwapChainCount;

	int iResolutionWidth;
	int iResolutionHight;
};