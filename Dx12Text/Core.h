#pragma once

#include "d3dx12.h"

#pragma comment(lib, "d3d12.lib")
#include <d3d12.h>

#pragma comment(lib, "dxgi.lib")
#include <dxgi1_4.h> 

#include <wrl.h>
using namespace Microsoft::WRL;

#include<vector>
using namespace std;

#include <DirectXMath.h>
using namespace DirectX;

#pragma comment(lib, "d3dcompiler.lib")
#include <D3Dcompiler.h>//