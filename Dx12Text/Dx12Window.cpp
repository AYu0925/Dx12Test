#include"Dx12Window.h"

LRESULT CALLBACK WindowMessgeProc(HWND i_hHwnd, UINT i_iMsg, WPARAM i_wParam, LPARAM i_lParam)
{
	switch (i_iMsg)
	{
	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;
	}

	//交给windows自己处理完成
	return DefWindowProc(i_hHwnd, i_iMsg, i_wParam, i_lParam);
}

BOOL CreateDx12Window(HINSTANCE i_hInstance, cDx12RenderConfig* io_cDx12RenderConfig)
{
	WNDCLASSEX cDx12Windows;
	cDx12Windows.cbSize = sizeof(WNDCLASSEX);
	cDx12Windows.cbClsExtra = 0;
	cDx12Windows.cbWndExtra = 0;
	cDx12Windows.hbrBackground = nullptr;
	cDx12Windows.hCursor = nullptr;
	cDx12Windows.hIcon = nullptr;
	cDx12Windows.hIconSm = NULL;
	cDx12Windows.hInstance = i_hInstance;
	cDx12Windows.lpszClassName = L"Dx12Test";
	cDx12Windows.lpszMenuName = nullptr;
	cDx12Windows.style = CS_VREDRAW | CS_HREDRAW;
	cDx12Windows.lpfnWndProc = WindowMessgeProc;

	ATOM aRegisterReturn = RegisterClassEx(&cDx12Windows);
	if (!aRegisterReturn)
	{
		return FALSE;
	}

	HWND hDx12WindowsHandle = CreateWindowEx(
		NULL,
		L"Dx12Test",
		L"Dx12Test",
		WS_OVERLAPPEDWINDOW,
		io_cDx12RenderConfig->iScrrenLeftX,
		io_cDx12RenderConfig->iScrrenLeftY,
		io_cDx12RenderConfig->iScrrenWidth,
		io_cDx12RenderConfig->iScrrenHight,
		NULL,
		nullptr,
		i_hInstance,
		NULL
	);

	if (!hDx12WindowsHandle)
	{
		return FALSE;
	}

	ShowWindow(hDx12WindowsHandle, SW_SHOW);

	UpdateWindow(hDx12WindowsHandle);

	io_cDx12RenderConfig->hHandle = hDx12WindowsHandle;

	return TRUE;
}